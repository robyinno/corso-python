import psycopg2

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='postgres'")
except:
    print "I am unable to connect to the database"

cur = conn.cursor()
namedict = ({"id":"1", "name":"Drake","age":12,"address":"via zippa","salary":120},
            {"id":"2", "name":"Foo","age":15,"address":"via zippa","salary":130},
            {"id":"3", "name":"Bar","age":16,"address":"via zippa","salary":1120})
cur.executemany("""INSERT INTO public.company(id, name, age,address,salary) VALUES (%(id)s, %(name)s,%(age)s,%(address)s,%(salary)s)""", namedict)

cur.execute("""SELECT id, name,address from public.company""")
rows = cur.fetchall()

for row in rows:
    print "id", row[0]
    print "name", row[1]
    print "address", row[2]
