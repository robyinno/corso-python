import cx_Oracle

connection = cx_Oracle.connect('system/oracle@//localhost:49161/xe')

cursor = connection.cursor()
cursor.execute("""INSERT INTO customers(customer_id,customer_name,city)
                VALUES (:customer_id,:customer_name,:city)""",
               {
                'customer_id' : 1,
                'customer_name' : 'Zelda',
                'city' : 'milano',
               }
              )
cursor.close()
connection.commit()
connection.close()
