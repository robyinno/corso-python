from os import getenv
import pymssql
import logging

from settings import *

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

conn = pymssql.connect(server, user, password, "mssqlcorso")
cursor = conn.cursor()
try:
    cursor.execute("""
    IF OBJECT_ID('persons', 'U') IS NOT NULL
        DROP TABLE persons
    CREATE TABLE guest.persons (
        id INT NOT NULL,
        name VARCHAR(100),
        salesrep VARCHAR(100),
        PRIMARY KEY(id)
    )
    """)
except pymssql.OperationalError, e:
    logger.exception(e)

try:
    cursor.executemany(
        "INSERT INTO guest.persons VALUES (%d, %s, %s)",
        [(1, 'John Smith', 'John Doe'),
         (2, 'Jane Doe', 'Joe Dog'),
         (3, 'Mike T.', 'Sarah H.')])
    conn.commit()
except pymssql.IntegrityError, e:
    logger.exception(e)
# you must call commit() to persist your data if you don't set autocommit to True

cursor.execute('SELECT * FROM guest.persons WHERE salesrep=%s', 'John Doe')
row = cursor.fetchone()
while row:
    print("ID=%d, Name=%s" % (row[0], row[1]))
    row = cursor.fetchone()

conn.close()

print "FINE"
