import logging
from time import sleep
from daemonize import Daemonize

pid = "/tmp/test3.pid"
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False
fh = logging.FileHandler("/tmp/test.log", "w")
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)
keep_fds = [fh.stream.fileno()]


def main():
    while True:
        logger.debug("Test")
        sleep(5)

daemon = Daemonize(app="test_app3", pid=pid, action=main, keep_fds=keep_fds)
daemon.start()
